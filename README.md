This is a VGM player which displays the YM2612's registers visually as a table.

This player does not support playback of samples. PSG is supported, but not displayed visually.

Included is a variety of custom VGM files.
