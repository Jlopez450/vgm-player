  include "ramdat.asm"
			  dc.l $FFFFFE,   start,     ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l HBlank,    ErrorTrap, VBlank,    ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.b "SEGA MEGASIS    "
              dc.b "(C) ComradeOj   "
              dc.b "ComradeOj's simple Genesis VGM driver           "
              dc.b "ComradeOj's simple Genesis VGM driver           "
              dc.b "GM 01234567-89"
              dc.w $DEAD        ;checksum
              dc.b "J               "
              dc.l 0
              dc.l ROM_End
              dc.l $FF0000
              dc.l $FFFFFF
              dc.b "    "
              dc.b "    "
              dc.b "    "
              dc.b "            "                           
              dc.b "This program contains blast processing! "
              dc.b "JUE             "
                          
start:        
		move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        

no_tmss:
	    move.w #$2700,sr
        bsr clear_regs
		bsr clear_ram
	    bsr setup_vdp
		bsr clear_vram	
		
		move.l #$C0000000,(a3)
		lea (palette),a5
		move.w #$2F,d4
		bsr vram_loop
		
        move.l #$40000010,(a3)   ;write to vsram    
		move.w #$FFFC,(a4)
		move.w #$FFFC,(a4)		
		
		lea (font),a5
		move.w #$0c00,d4
		move.l #$40000000,(a3)
		bsr vram_loop
		
		lea (overlay),a5
		move.l #$60000002,(a3)		
		bsr smalltextloop
		
		lea (music)+66,a0
		move.w #$100,($A11100)
		move.w #$100,($A11200)
		move.w #$2300,sr
loop:
		bra loop
		
music_driver:
		lea (YM_buffer1),a1
		lea (YM_buffer2),a2
		moveq #$00000000,d0
		moveq #$00000000,d6
		bsr test2612	
        move.b (a0)+,d0
		cmpi.b #$61,d0
		 beq wait		
		 cmpi.b #$66,d0
		 beq loop_playback	
		cmpi.b #$52,d0 
		 beq update2612_0
		cmpi.b #$53,d0 
		 beq update2612_1
		cmpi.b #$50,d0
		 beq update_psg
		bra music_driver
	
update2612_0:
	    move.b (a0)+,d6
	    move.b d6,$A04000
		add.l d6,a1
        move.b (a0), (a1)
        move.b (a0)+, $A04001
        bra music_driver
		
update2612_1:	
	    move.b (a0)+,d6
	    move.b d6,$A04002
		add.l d6,a2
        move.b (a0), (a2)
        move.b (a0)+, $A04003
		bra music_driver
		
loop_playback:
 		move.b #$9f,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011		
		lea (music)+66,a0
		bra music_driver
	
update_psg:
        move.b (a0)+,$C00011
		bra music_driver
	
wait:
		rts
	    
clear_regs:
        move.l #$00000000,d0
        move.l #$00000000,d1
        move.l #$00000000,d2
        move.l #$00000000,d3
        move.l #$00000000,d4
        move.l #$00000000,d5
        move.l #$00000000,d6
        move.l #$00000000,d7
        move.l #$00000000,a0
        move.l #$00000000,a1
        move.l #$00000000,a2
        move.l #$00000000,a3
        move.l #$00000000,a4
        move.l #$00000000,a5
        move.l #$00000000,a6
        rts
test2612:
		clr d0
        move.b $A04001,d0
		andi.b #$80,d0
        cmpi.b #$80,d0
		beq test2612 
		rts	

clear_vram:		       
        move.l  #$40000000,(a3)  ;set VRAM write    
        move.w  #$7fff, d4
clear_loop:             
        nop
        move.w  #$0000,(a4)       ;clear vram
        dbf d4,clear_loop
        rts
setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
        dbf d4,VDP_Loop
        rts 
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts 		

smalltextloop:
		move.w #$0027,d4		;draw 40 cells of text
		clr d5	
textloop:
		move.b (a5)+,d5	
		cmpi.b #$25,d5			;% (end of text flag)
		 beq return	
		cmpi.b #$20,d5
		 beq clearspace			;band-aid fix
clear_return:		 
		andi.w #$00ff,d5
		add.w #$0040,d5
        move.w d5,(a4)	
		dbf d4,textloop	
		move.w #$0017,d4	    ;draw 24 cells of nothing
emptyloop:		
		move.w #$0000,(a4)
		dbf d4,emptyloop
		bra smalltextloop
clearspace:
		move.b #$00,d5
		bra clear_return
		
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
		
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
return:
		rts

ErrorTrap:        
        nop
        bra ErrorTrap


HBlank:           ;unused
        nop
        rte

VBlank:           ;unused
        bsr music_driver
		bsr render
        rte
		
render:
		moveq #$00000000,d2
		lea (YM_buffer1),a1
		move.l #$0000A10A,d5 ;starting location	
		move.w #$000b,d3
		bsr renderloop
		
		moveq #$00000000,d2
		lea (YM_buffer2),a1
		move.l #$0000A78A,d5 ;starting location	
		move.w #$000b,d3
		bsr renderloop		
		rts
		
		
renderloop:	
		move.w #$0007,d4		
		move.l d5,d0
		bsr calc_vram	
		move.l d0,(a3)
rendersub:	
		clr d7	
		move.b (a1)+,d2		
		move.b d2,d7
		andi.b #$f0,d7
		lsr.w #$04,d7
		add.w #$0030,d7
        move.w d7,(a4)	
		clr d7	
		move.b d2,d7
		andi.b #$0f,d7
		add.w #$0030,d7
        move.w d7,(a4)
		clr d7	
		move.b (a1)+,d2		
		move.b d2,d7
		andi.b #$f0,d7
		lsr.w #$04,d7
		add.w #$2030,d7
        move.w d7,(a4)	
		clr d7		
		move.b d2,d7
		andi.b #$0f,d7
		add.w #$2030,d7
        move.w d7,(a4)	
		dbf d4, rendersub	
		add.l #$0080,d5
		dbf d3, renderloop
		rts

		
VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ; Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8228	;field A    
	dc.w $8300	;$833e	
	dc.w $8407	;field B	
	dc.w $8578	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8Aff		
	dc.w $8B00		
	dc.w $8C81	;81 normal 89 shadow and highlight
	dc.w $8D30  ;h scroll		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
palette:
	dc.w $0000, $0000, $0000, $0EEE, $0EEE, $0EEE, $0EEE, $0EEE
	dc.w $0EEE, $0EEE, $0EEE, $0EEE, $0EEE, $0EEE, $0EEE, $0EEE
	dc.w $0000, $0000, $0000, $0AAA, $0AAA, $0AAA, $0AAA, $0AAA
	dc.w $00AA, $0AAA, $0AAA, $0AAA, $0AAA, $0AAA, $0AAA, $0AAA
	
font:	
	incbin "hexascii.bin"
	incbin "ascii.bin"
	
overlay:
	dc.b " ComradeOj's simple Genesis VGM driver  "
	dc.b "     0 1 2 3 4 5 6 7 8 9 A B C D E F    "
	dc.b " $00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Y "
	dc.b " $10 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX M "
	dc.b " $20 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 2 "
	dc.b " $30 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 6 "
	dc.b " $40 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 1 "
	dc.b " $50 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 2 "
	dc.b " $60 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX   "
	dc.b " $70 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX P "
	dc.b " $80 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX A "
	dc.b " $90 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX R "
	dc.b " $A0 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX T "
	dc.b " $B0 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 1 "
	dc.b "     0 1 2 3 4 5 6 7 8 9 A B C D E F    "
	dc.b " $00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Y "
	dc.b " $10 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX M "
	dc.b " $20 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 2 "
	dc.b " $30 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 6 "
	dc.b " $40 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 1 "
	dc.b " $50 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 2 "
	dc.b " $60 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX   "
	dc.b " $70 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX P "
	dc.b " $80 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX A "
	dc.b " $90 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 2 "
	dc.b " $A0 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX T "
	dc.b " $B0 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 2%"
	dc.b "                                        "	

music:
    incbin "output.vgm"
ROM_End:
              
              